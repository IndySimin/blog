---
title: Logboek
date:2017-09-20
---


# Wo 20 sep. 17
-----
-----

-	Dit is weer een studio dag. De lange dagen met veel brainstorming. Blockparty begon de dag een beetje stroef. Na de Kill Your Darling moest er namelijk een heel nieuw concept bedacht worden. Ik speelde vroeger altijd veel games met het Butterfly effect omdat ik het altijd heel spannend en leuk vond om mijn eigen personage te sturen. Ik wilde dit graag in de game hebben dus heb ik dit idee op tafel gelegd. Er kwam direct een vraag in me op: wat nou als ik twee bestaande games die door iedereen makkelijk te begrijpen is koppel aan elkaar? Ik kwam uit op ''Wie ben ik?''. Iedereen kent t, jong en oud. Ik kwam met het idee om het Butterfly effect van een game te mixen met het spel ''Wie ben ik?''. 
De interpretatie van de doelgroep hebben we wat veranderd. De studenten met een museum kaart houden allemaal van kunst, anders neem je geen museumkaart, dus hebben we iets meer gedacht in de richting van: de kunst liefhebbende studenten. Zo kregen we meer opties voor onszelf om een leuke game te bedenken. Ons nieuwe concept zal worden uitgelegd in de presentatie. Ik vind persoonlijk dit concept veel leuker omdat het niet zomaar even een korte tour door Rotterdam is of een speurtocht. Het zijn leuke activiteiten die elkaar verbindt met Rotterdam en met elkaar. De Kill Your Darling heeft toch nog iets goeds opgeleverd. Ook bedachten we dat de connectie van mensen veel sneller gaat wanneer er fanatiek wordt gespeelt (dit werd bedacht tijdens mijn eigen spelletje tafelvoetbal). Dit komt omdat je jezelf dan al laat zien van een hele open kant. Bijvoorbeeld: tafelvoetbal, iedereen kent het maar zodra we spelen hebben we allemaal de drang om te winnen, hoe dan ook! Iedereen schreeuwt en juicht gezellig mee. Zo heb je nog veel meer spellen. Dit wilde wij er ook graag in verwerken. 

-	Vervolgens had ik een workshop waarbij je je boek “CREATIVITEIT HOE? ZO!” nodig had. Ik vond persoonlijk de workshop niet veel informatie bevatten. Ik heb van tevoren het boek al een beetje doorgekeken en er stonden veel interessante dingen in zoals: hoe werken de hersenen bij het vormen van creatieve gedachten? Dit leek me leuk. Het enige wat we in de les kregen was de informatie over divergeren en convergeren die we de dag ervoor ook al hadden gehoord. Voor mij stelde deze workshop niks voor.

-	Aan het eind van de dag hebben wij studio coaching gekregen waarbij we een rug steuntje kregen voor het maken van ons beroepsprofiel. We moesten een typische CMD studetn beschrijven. Ik vond dit een vreemde opdracht aangezien je mensen zo in hokjes plaatst. Alleen al aan onze tafel van vijf personen zaten al vijf verschillende persoonlijkheden, uiterlijken en interesses. Maar we hebben de opdracht alsnog gedaan natuurlijk. Laat op de dag kregen we pas echt te horen wat nou eigenlijk de opdracht was van het beroepsprofiel. 
