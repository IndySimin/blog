---
title: Logboek
date: 2017-09-19
---


# Di 19 sep. 17
-----
-----

Het hoorcollege over prototyping vond ik erg goed vooral doordat de docent steeds vergelijkingen trok met wat wij ook zo nu en dan gebruiken of bedenken. Interspection illusion vond ik erg interessant omdat wij inderdaad dingen mooi vinden dat is gebaseerd op onze emotie of interesse. Emotie en rationaal ligt soms ver uit elkaar. Het kan namelijk één van de grote interesses zijn maar dat hoeft niet altijd de juist oplossingsmanier te zijn. Wij zijn er dus voor om dit goed te onderscheiden.

[Dubble diamnond](https://goo.gl/images/b5R5jp)

Dit is een handig systeem dat uitlegt hoe de divergerende en convergerende processen werken. 
Om tot een conclusie te komen zullen we eerst veel ideeën op tafel moeten leggen om vervolgens iets te kiezen. Dit kan gebeuren door allerlei soorten brainstorm methodes. 
We moeten komen/blijven tot/bij de kern. Het is leuk dat we er allemaal extra tools bij bedenken maar als de basis niet werkt zal je toch echt iets anders moeten bedenken. Dit zal de eerste paar keren natuurlijk fout gaan maar daarom zal je altijd je prototype moeten testen op jou doelgroep. 

