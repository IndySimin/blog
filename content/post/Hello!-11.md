---
title: Logboek
date: 2017-09-11
---



#Ma 11 sep 
-----------
-----------

- spel bedenken om die museum match. We wilde dit als hoofd bestand nemen om mensen te verbinden en daaromheen een spel bedenken omdat uit de interviews kamen dat onze doelgroep niet graag wilde chatten tijdens een game. 

-	We begonnen de dag met de stand-up waar ik helaas niet aanwezig kon zijn. Wel kreeg ik de feedback te horen van Blockparty. We moesten meer onderzoek verrichten zoals interviews en betere enquête resultaten omdat we een concept hadden maar geen duidelijke resultaten wat ons specifiek gekozen doelgroep nou eigenlijk leuk vind. 
-	De blog workshop, wat een expeditie was dat voor mij en nog steeds werkt mijn blog niet. Eerst had ik mijn blog af maar had hem vervolgens per ongeluk verwijderd na het helemaal geïnstalleerd te hebben. Mijn teamgenoten hebben we er wel mee geholpen gelukkig want zelf was ik er nooit uitgekomen. Nu heb ik een paar dingen geüpload maar de datum staat nog steeds niet goed. Mijn blog mooi maken doe ik in het weekend tussendoor hou ik het nog even bij in Word.
-	Leroy en Femke zijn naar Willem de Kooning gegaan en hebben daar wat interviews afgelegd. Ik heb de opnames beluisterd. Ik vond de vragen onduidelijk gesteld maar er kwam wel bruikbare informatie uit en ideeën van kunstenaars op Willem de Kooning om ons te helpen die ik erg leuk vond zoals: de mensen zelf een kunst stuk laten beschrijven waarbij bepaalde aspecten wel erin vernoemd moeten worden. Ook zijn er wat Engelse studenten ondervraagt. Dit bracht mij op het idee om de app wat internationaler te maken. Rotterdam is tenslotte een multiculturele stad. De app moet voor iedereen toegankelijk zijn. We kwamen er ook achter wat ze echt niet zien zitten in een spel. Ik maak de spelanalyse dus zal ik deze informatie moeten gebruiken om een goed geheel ervan te maken 

>> Ik wilde vandaag een start maken aan mijn individuele deliverables. Ik heb dit niet gedaan omdat ik het editen nog niet goed onder de knie heb en dit liever thuis uitvogel.

>Mijn planning voor deze week:
Dinsdag 12: deliverables maken ( onderzoek visueel maken + moodboard)
Woensdag 13: workshop volgen over het moodboard
Donderdag 14: tools for design beginnen, spelanalyse maken, team deliverables bespreken
Vrijdag: deliverables af hebben!!
