---
title: logboek
date: 2017-09-06
---


# Wo 6 sep 
---------- 
----------
- Design challenge De enquête voor ons doelgroep onderzoek heb ik afgerond. Ook de enquête heb ik vorm gegeven. De kleuren heb ik aangepast aan ons (vandaag gemaakte) logo. Dit is onze enquête geworden: https://surveys.enalyzer.com/?pid=s4c4ha7g Ik heb er ook voor gezorgd dat er een barcode is voor de enquête zodat we het op flyers kunnen plaatsen om zo een betere feedback te krijgen van onze doelgroep om de app te blijven verbeteren en nieuwere versies van te maken. 
- Vandaag wilde we hem gaan delen maar waarschijnlijk delen we hem morgen pas op Facebook. 
- Workshop van het blog We kregen een workshop over HTML. Ik heb nog nooit gehoord over alles wat er werd verteld dus ik heb er wel weer wat bij geleerd. het belangrijkste was dat we er een blog mee konden maken. Ik kom van het HAVO dus ik ben de enige die er niks van af wist maar gelukkig heeft me groep (Blockparty) mij ermee geholpen. Nu lukt het me ook alleen om mijn blog zelf af te maken met alle versieringen van zich. Ik ben opgelucht dat ik dit heb gekregen (aangezien ik van het HAVO kom). Momenteel heb ik geen enkel idee over illustreren, Indesign, Adobe of andere programma's maar heb me voor de tools for design ingeschreven. Heb zeker wat geleerd en hou ook alles netjes bij in het blog.
- Als afsluiter kregen we les van twee studie coaches die ons willen helpen begeleiden met de groep en onze samenwerking. Onze samenwerking met mijn groep verliep al vlekkeloos dus veel was er niet voor nodig. We hebben dezelfde interesses en we vullen elkaar goed aan met creatieve ideeën. 

> Studie coaching: 
> *communicatie*: Wij communiceren en sturen alles door via Google drive. iedereen kan daarin bestanden aanvullen. Als er kleine afspraken zijn doen we dit d.m.v Whats app 
*Belangen*: persoonlijke belangen, we hebben allemaal veel ideeën die we willen uitvoeren maar dat kan niet allemaal in één keer gebeuren. Belangen van de opdrachtgever, dat zo veel mogelijk eerstejaarsstudenten in contact met elkaar raken d.m.v./en Rotterdam erbij te betrekken. 
*Organistatie*: per dag verdelen we de taken en wat er gaat gebeuren. Zo deed ik vandaag met Leroy de enquête, Janice maakte het logo met Femke en Eva werkte aan de deliverbles. Eva is onze team captian en houd onze ideeën binnen het kader van het project en ons concept. als iemand zich niet aan de deadlines houd kunnen we hooguit een waarschuwing geven meer niet en anders accepteren we maar dat het verder moet met 4 man ondanks dat die persoon er wel in blijft. 
*Doelen*: Ik wil bereiken dat ik overweg kan met design programma's ik wil me team beter helpen met de creatie zelf dan alleen het onderzoek uitpluizen en teksten typen. Het groepsdoel: ons concept zo goed mogelijk uitwerken en onszelf uitgang door een kleine groep studenten uit te kiezen als doelgroep. het Project is geslaagd als we allemaal tevreden zijn. Veel van ons zijn perfectionistisch dus dat wel duren tot de echte deadline er aankomt. 
