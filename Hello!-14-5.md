---
title: logboek
---


# Do 14 sep. 17
---------
---------

Vandaag begonnen onze design tools, mijn gekozen onderwerp was Illustrator. Ik heb weinig ervaring met design tools dus wil ik ook gelijk het grote en meest gebruikte onderwerp van de tools aanpakken. De eerste les was voor mij wat nutteloos maar natuurlijk moet je beginnen met de basics. Ik ben namelijk thuis al begonnen met een beetje te begrijpen wat bepaalde knoppen in illustrator doen en hoe ze werken . Ook heb ik geprobeerd een eigen logo te maken. Heel mooi is het natuurlijk niet maar hoe verder ik kom met de lessen van illustrator hoe verder ik mijn eigen logo wil ontwikkelen. 

Vervolgens hadden wij een werkcollege over b het hoorcollege: Beeldspraak. We leerde de theorie toepassen in de praktijk. Het zijn altijd simpele opdrachten maar het helpt mij wel met de theorie beter te begrijpen door het ook echt uit te voeren. Ik vind het soms wat langdradig maar ik snap wel waarom het wordt gedaan. 

Na de lessen heb ik nog met het team ons prototype getest waarbij we erachter kwamen dat we nog een aantal aspecten miste zoals: de opstelling van bepaalde pictogrammen wat zorgt voor onduidelijkheid en nog wat 'skip', 'volgende' of 'gereed' knoppen. Voor de rest was alles goed uitgedacht en verwerkt. We hebben het prototype bij mij, iemand van buiten onze school en een mede student getest om een zo goed mogelijk beeld te krijgen van onze game zonder enige vooroordelen, interesses of ander soort emotie.

¬	Mijn planning voor vrijdag: deliverables afmaken en het vervolg van het concept aanpassen naar de huidige status na onze feedback

